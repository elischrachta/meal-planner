import React from 'react';
import RootRouter from './domain/RootRouter';
import Header from './domain/Header';

const App = () => {
	return (
		<div>
			<Header />
			<RootRouter />
		</div>
	);
};

export default App;
