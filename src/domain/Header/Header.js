import { makeStyles } from '@material-ui/core';
import { A, usePath } from 'hookrouter';
import React from 'react';

const useStyles = makeStyles({
	root: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
});

const Header = props => {
	const classes = useStyles(props);

	return (
		<header className={classes.root}>
			<h1>
				<A href="/">MP</A>
			</h1>
			{usePath() === '/go-shopping' ? null : <A href="/go-shopping">Go shopping</A>}
		</header>
	);
};

export default Header;
