import React from 'react';

const NotFound = () => <p>{"The page you're looking for doesn't exist"}</p>;

export default NotFound;
