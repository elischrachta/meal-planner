import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { register as registerSW } from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

registerSW();
