module.exports = {
	env: {
		browser: true,
	},
	parserOptions: {
		ecmaVersion: 2020,
		sourceType: 'module',
	},
	extends: ['eslint:recommended', 'plugin:react/recommended'],
	plugins: ['prettier', 'react-hooks'],
	rules: {
		'prettier/prettier': 'warn',
		'react-hooks/rules-of-hooks': 'error',
		'react-hooks/exhaustive-deps': 'warn',
	},
	settings: {
		react: {
			version: 'detect',
		},
	},
	overrides: [
		{
			files: ['*/serviceWorker.js'],
			env: {
				serviceworker: true,
			},
			globals: { process: true },
		},
		{
			files: ['*.spec.js*'],
			env: {
				jest: true,
			},
		},
	],
};
